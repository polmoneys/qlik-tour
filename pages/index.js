import styles from "../styles/Home.module.css";
import Link from "next/link";
import Head from "next/head";

// import styles from '../styles/Home.module.css'
// className={styles.container}

import { HeroSection } from "../sections/hero";
import { EventSection } from "../sections/event";
import { EnrollSection } from "../sections/enroll";
import { ContactSection, NewsletterSection } from "../sections/contact";
import { VenueSection } from "../sections/event/venue";

import { Header } from "../units/header";
import { Footer } from "../units/footer";
// import { useTapActive } from "../units/use-tap-active";

export default function Home() {
  return (
    <main className={styles.main}>
    <p>soon</p>
      {/* <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Header />
      <main>
        <HeroSection />
        <EventSection />
        <EnrollSection />
        <VenueSection />
        <ContactSection />
      </main>
      <Footer>
        <NewsletterSection />
      </Footer> */}
    </main>
  );
}

const SITE_CONSTANTS = {
  title: "",
  description: "",
  keywords: "",
  copyright: "",
  contact: "",
  url: "",
};

const Seo = () => {
  return (
    <>
      <meta charset="utf-8" />
      <title>Pol Moneys</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta
        name="description"
        content="Poise for hire. My materials are code and stories, two of the most powerful forces of our time."
      />
      <meta name="keywords" content="KEYWORD KEYWORD KEYWORD" />
      <meta name="contact" content="email@email.com" />
      <meta name="copyright" content="WHO ? " />
      {/* <meta name="theme-color" content="#686969" />
<link rel="canonical" href="" />
    <meta name="googlebot" content="index,follow" />
    <meta name="robots" content="index,follow" />
    <meta name="rating" content="general" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-title" content="Pol Moneys" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#686969" />

    <link rel="apple-touch-icon" href="./media/icon.png" />
    <link rel="icon" sizes="192x192" href="./media/icon.png" />
    <link rel="shortcut icon" href="./media/favicon.png" /> */}

      {/* OpenGraph tags 
      <meta name="og:url" content={url} />
      <meta name="og:title" content={title} />
      <meta name="og:description" content={description} />
      <meta name="og:image" content={image} />
      <meta name="og:type" content="website" />
      <meta name="fb:app_id" content={facebook.appId} />
      */}
      {/* Twitter Card tags 
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      <meta name="twitter:card" content="summary" />
      <meta
        name="twitter:creator"
        content={config.authorTwitterAccount ? config.authorTwitterAccount : ""}
      />
      */}

      <link
        href="https://fonts.googleapis.com/css?family=Quicksand:500,700&display=swap"
        rel="stylesheet"
      />
    </>
  );
};
