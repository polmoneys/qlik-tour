import { HoodPostcard, CityPostcard, VenuePostcard } from "../units/postcard";
import { Spacer } from "../units/spacer";

export default function Home() {
  return (
    <main className="flex col cross-center">
      <Spacer size="xl" />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <CityPostcard />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <VenuePostcard />
      <Spacer size="xl" />
      <Spacer size="xl" />
      <HoodPostcard />
      <Spacer size="l" />
    </main>
  );
}
