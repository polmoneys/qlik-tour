import "../styles/globals.css";

import "../sections/hero/hero.css";
import "../sections/enroll/enroll.css";
import "../sections/event/event.css";
import "../sections/enroll/tabs.css";

import "../units/action/action.css";
import "../units/footer/footer.css";
import "../units/forms/forms.css";
import "../units/header/header.css";
import "../units/pic/pic.css";
import "../units/postcard/postcard.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
