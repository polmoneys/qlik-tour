import PropTypes from "prop-types";
import { default as __ } from "./utils";

const FormPropTypes = {
  a11yTitle: PropTypes.string.isRequired,
  // children: PropTypes.oneOfType([
  //   PropTypes.arrayOf(PropTypes.element),
  //   PropTypes.element,
  // ]).isRequired,
  tracking: PropTypes.object,
  formId: PropTypes.string,
  columns: PropTypes.number,
  server: PropTypes.object,
  report: PropTypes.func,
  theme: PropTypes.string,
};

const FormDefaultProps = {
  columns: null,
  a11yTitle: null,
  children: null,
  tracking: {},
  formId: __.makeId(),
  server: null,
  report: __.noop(),
  theme: "light",
};

export { FormPropTypes, FormDefaultProps };

const InputPropTypes = {
  autoComplete: PropTypes.string,
  cantBeBlank: PropTypes.bool,
  // label: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.node,
  //   PropTypes.string
  // ]).isRequired,
  minChars: PropTypes.number,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onPaste: PropTypes.func,
  optional: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

const InputDefaultProps = {
  autoComplete: "off",
  cantBeBlank: false,
  label: "",
  minChars: 3,
  onBlur: __.noop(),
  onFocus: __.noop(),
  onPaste: __.noop(),
  optional: false,
  disabled: false,
  type: "text",
};

export { InputDefaultProps, InputPropTypes };

const CheckBoxPropTypes = {
  // label: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.node,
  //   PropTypes.string,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]).isRequired,
  icon: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
    PropTypes.instanceOf(PropTypes.Bool),
  ]),
  disabled: PropTypes.bool,
};

const CheckBoxDefaultProps = {
  disabled: false,
  icon: false,
  label: "",
};

export { CheckBoxPropTypes, CheckBoxDefaultProps };

const RadioPropTypes = {
  //   label: PropTypes.oneOf([PropTypes.element, PropTypes.node, PropTypes.string])
  //     .isRequired,
  disabled: PropTypes.bool,
  labelGroup: PropTypes.string,
};

const RadioDefaultProps = {
  disabled: false,
  label: "",
  labelGroup: "",
};

export { RadioPropTypes, RadioDefaultProps };

const SwitchPropTypes = {
  label: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
    PropTypes.string,
  ]),
  onColor: PropTypes.string,
  offColor: PropTypes.string,
  disabled: PropTypes.bool,
  // onIcon: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]),
  // offIcon: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]),
  offCb: PropTypes.func,
  onCb: PropTypes.func,
};

const SwitchDefaultProps = {
  disabled: false,
  label: "",
  onColor: "tomato",
  offColor: "#242424",
  onIcon: false,
  offIcon: false,
  offCb: __.noop(),
  onCb: __.noop(),
};
export { SwitchPropTypes, SwitchDefaultProps };
