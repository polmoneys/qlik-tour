import { useLayoutEffect, useState } from "react";

/* eslint-disable */
const unwrapXS = (arg) => (Array.isArray(arg) ? arg[0] : arg);

const compactObject = (obj) => {
  let newObj = {};
  Object.keys(obj).forEach((prop) => {
    if (obj[prop] !== "") {
      newObj[prop] = obj[prop];
    }
  });
  return newObj;
};
export { unwrapXS, compactObject };

const noop = () => {};

function cbToCb(cb) {
  return typeof cb === "function" ? cb : noop;
}

const makeId = () => Math.random().toString(36).substring(2, 15);

function isString(obj) {
  return Object.prototype.toString.call(obj) === "[object String]";
}

const getProp = (prop) =>
  getComputedStyle(document.documentElement, null).getPropertyValue(prop);

function componentStylesLoaded(query) {
  let cssVar = getProp(query);
  return cssVar.length !== 0;
}

export default {
  makeId,
  noop,
  cbToCb,
  componentStylesLoaded,
  isString,
};
