import React, {
  useEffect,
  useState,
  cloneElement,
  isValidElement,
} from "react";
import { FormPropTypes, FormDefaultProps } from "../types";

const { map } = React.Children;

function Form({ children, tracking, ...otherProps }) {
  const [fields, updateForm] = useState(tracking);

  const { report } = otherProps;
  useEffect(() => {
    report && report(fields);
  }, [fields]);

  const onUpdate = (k, v) => {
    updateForm((fields) => ({
      ...fields,
      [k]: v,
    }));
  };

  const { server } = otherProps;
  const spreadProps = (c) => {
    const { props } = c;
    return {
      ...props,
      initial: fields[props.name],
      server: server && server.type === props.name ? server : null,
      onUpdate,
    };
  };

  const {
    a11yTitle,
    columns = false,
    gap = 0,
    formId,
    ...anyPropLeft
  } = otherProps;

  // if (CSS.supports("display: grid")){}

  return (
    <>
      <form id={formId} aria-label={a11yTitle} {...anyPropLeft}>
        {map(children, (c) => {
          if (!isValidElement(c) || typeof c.type === "string") {
            return c;
          }
          return cloneElement(c, spreadProps(c));
        })}
      </form>
    </>
  );
}

Form.displayName = "DefaultsForm";
Form.propTypes = FormPropTypes;
Form.defaultProps = FormDefaultProps;

export default Form;
