import * as yup from "yup";

const email = yup
  .string()
  .email("Email looks invalid")
  .required("Can not be empty");

const password = yup
  .string()
  .min(6, "6 chars min")
  .required("Password required");

const url = yup.string().url();

const username = yup.string().max(100, "100 chars max").required();

const requiredPositiveNumber = yup
  .number()
  .integer("Must be a number")
  .positive("Must be a positive number")
  .required()
  .typeError("Please provide a number");

const passwordSchema = yup.object().shape({
  password: yup.string().min(6, "Six chars min").required("Password required"),
  confirmation: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match"),
});
export default {
  email,
  password,
  passwordSchema,
  requiredPositiveNumber,
  url,
  username,
};

/*

function merge(...schemas) {
  const [first, ...rest] = schemas;
  const merged = rest.reduce(
    (mergedSchemas, schema) => mergedSchemas.concat(schema),
    first
  );
  return merged;
}

"alt" export const mergedSchema = emailSchema.concat(urlSchema);

array().of(lazy(value => {
     if (value.type === 'bike') return bikeSchema
}))
*/
