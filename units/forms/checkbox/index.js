import React, { useEffect, useState } from "react";
import { CheckBoxPropTypes, CheckBoxDefaultProps } from "../types";
import { default as __ } from "../utils";
import { CheckboxLabel } from "../labels";

function CheckBox({ initial, name, onUpdate, ...otherProps }) {
  const [selectedCheckBox, setSelectedValue] = useState(initial);

  useEffect(() => {
    onUpdate(name, selectedCheckBox);
  }, [selectedCheckBox]);

  const onChangeUpdate = (e) => {
    setSelectedValue(e.target.checked);
  };

  const { label } = otherProps;

  // const toggle = () => {
  //   setSelectedValue(!selectedCheckBox);
  // };

  const inputLabel = __.isString(label) ? (
    <CheckboxLabel name={name} label={label} />
  ) : (
    label({
      checked: selectedCheckBox,
      name,
    })
  );

  const { icon } = otherProps;
  const inputIco = icon ? (
    icon({
      checked: selectedCheckBox,
      name,
    })
  ) : (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      style={{
        left: "4px",
        display: selectedCheckBox ? "block" : "none",
      }}
    >
      <path d="M9.86 18a1 1 0 0 1-.73-.32l-4.86-5.17a1 1 0 1 1 1.46-1.37l4.12 4.39 8.41-9.2a1 1 0 1 1 1.48 1.34l-9.14 10a1 1 0 0 1-.73.33z" />
    </svg>
  );

  const { disabled } = otherProps;

  return (
    <div className="checkbox-container">
      {inputIco}

      <input
        name={name}
        value={name}
        checked={selectedCheckBox}
        disabled={disabled}
        type="checkbox"
        onChange={onChangeUpdate}
      />
      {inputLabel}
    </div>
  );
}

CheckBox.displayName = "DefaultsCheckBox";
CheckBox.propTypes = CheckBoxPropTypes;
CheckBox.defaultProps = CheckBoxDefaultProps;

export default CheckBox;
