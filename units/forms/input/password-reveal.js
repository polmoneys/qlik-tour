import React, { useState } from "react";
import { default as VALIDATORS } from "../validations";
import Input from "./index";

function InputWithPassword(props) {
  const [shown, setShown] = useState(false);
  return (
    <div className="flex main-between cross-center row-gap">
      <div style={{ flex: 1 }}>
        <Input
          type={shown ? "text" : "password"}
          validation={VALIDATORS.password}
          {...props}
        />
      </div>
      <button type="button" onClick={() => setShown(!shown)}>
        Show/Hide
      </button>
    </div>
  );
}

export { InputWithPassword };
