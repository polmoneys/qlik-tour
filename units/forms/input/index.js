import React, { useEffect, useRef, useState } from "react";
import { InputDefaultProps, InputPropTypes } from "../types";
import { default as __ } from "../utils";
import { default as VALIDATORS } from "../validations";
import { InputAdmin } from "./admin";
import { InputLabel } from "../labels";
import { InputWithPassword } from "./password-reveal";

function Input(props) {
  const { initial, name, ...otherProps } = props;
  const [typing, setInputValue] = useState(initial);
  const [error, setError] = useState(false);
  const inputElement = useRef();
  const [capsLockOn, setCapsLock] = useState(false);

  useEffect(() => {
    const { autofocus } = otherProps;
    if (autofocus) inputElement.current.focus();
  });

  useEffect(() => {
    const { minChars, validation, cantBeBlank = false } = otherProps;
    if (typing.length < minChars && cantBeBlank) {
      setError({
        type: name,
        msg: "Can not be empty",
      });
      return;
    } else {
      setError(false);
    }
    if (validation) {
      validation
        .validate(typing)
        .then(() => {
          // eslint-disable-line
          setError(false);
          onUpdate(name, typing);
        })
        .catch((ValidationError) => {
          setError({
            type: name,
            msg: ValidationError.message,
          });
        });
    } else {
      onUpdate(name, typing);
    }
  }, [typing]);

  const onChangeUpdate = (e) => {
    e.persist();
    // disabled at codesandbox
    // setCapsLock(e.getModifierState("CapsLock"));
    setInputValue(e.target.value);
  };

  /*   const onPasteUpdate = e => {
    e.persist();
    const pasted = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, pasted); // eslint-disable-line
  };
 */
  const {
    disabled,
    autoComplete,
    label,
    onBlur,
    onFocus,
    // onPaste,
    onUpdate,
    server,
    type,
    cantBeBlank,
  } = otherProps;

  const inputLabel = __.isString(label) ? (
    <InputLabel {...props} />
  ) : (
    label({
      value: typing,
      name,
      error,
      htmlFor: name,
      capsLockOn,
    })
  );

  const clss = getClasses(props);
  return (
    <div className="input-container">
      {inputLabel}

      <input
        ref={inputElement}
        value={typing}
        type={type}
        name={name}
        className={clss}
        disabled={disabled}
        autoComplete={autoComplete}
        onChange={onChangeUpdate}
        onFocus={onFocus ? onFocus : __.noop()}
        onBlur={onBlur ? onBlur : __.noop()}
        // onPaste={onPaste ? onPaste : onPasteUpdate}
      />

      {error && (
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      )}
      {server && (
        <p aria-live="polite" input-error="">
          {server.msg}
        </p>
      )}
    </div>
  );
}

function getClasses(props) {
  return [props.className, props.s && "input-s", props.xs && "input-xs"]
    .filter(Boolean)
    .join(" ");
}

Input.ReadOnly = (props) => <Input type="text" readonly {...props} />;

Input.Email = (props) => (
  <Input type="email" validation={VALIDATORS.email} {...props} />
);

Input.Password = (props) => <InputWithPassword {...props} />;

Input.Admin = (props) => <InputAdmin type="text" {...props} />;

Input.Username = (props) => (
  <Input type="text" validation={VALIDATORS.username} {...props} />
);

Input.Url = (props) => (
  <Input type="text" validation={VALIDATORS.url} {...props} />
);

Input.Number = (props) => (
  <Input type="text" inputmode="numeric" pattern="[0-9]*" {...props} />
);

Input.displayName = "DefaultsInput";
Input.propTypes = InputPropTypes;
Input.defaultProps = InputDefaultProps;

export default Input;
