import React, { useEffect, useRef, useState } from "react";
import { default as __ } from "../utils";
import { InputLabel } from "../labels";

function InputAdmin(props) {
  const { initial, name, ...otherProps } = props;
  const [typing, setInputValue] = useState(initial);
  const [error, setError] = useState(false);
  const [touched, setTouched] = useState(false);
  const [newIsNotOld, setHasChanged] = useState(false);
  const inputElement = useRef();
  const oldValue = useRef(initial);
  const [capsLockOn, setCapsLock] = useState(false);

  const { autofocus } = otherProps;
  useEffect(() => {
    if (autofocus) inputElement.current.focus();
  });

  useEffect(() => {
    const { minChars, validation, cantBeBlank } = otherProps;
    if (typing.length < minChars && cantBeBlank) {
      setError({
        type: name,
        msg: "No puede estar vacÃÂ­o",
      });
      return;
    } else {
      setError(false);
    }
    if (validation) {
      validation
        .validate(typing)
        .then(() => {
          // eslint-disable-line
          setError(false);
          onUpdate(name, typing);
        })
        .catch((ValidationError) => {
          setError({
            type: name,
            msg: ValidationError.message,
          });
        });
    } else {
      if (initial !== typing) {
        setTouched(true);
      }
      onUpdate(name, typing);
    }
  }, [typing]);

  const onChangeUpdate = (e) => {
    e.persist();
    // disabled at codesandbox
    // setCapsLock(e.getModifierState("CapsLock"));
    setInputValue(e.target.value);
    if (e.target.value !== oldValue.current) {
      setHasChanged(true);
    }
  };

  const onPasteUpdate = (e) => {
    e.persist();
    const pasted = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, pasted); // eslint-disable-line
  };

  const undo = () => {
    setInputValue(oldValue.current);
  };
  const {
    autoComplete,
    label,
    onBlur,
    onFocus,
    onPaste,
    onUpdate,
    optional,
    server,
    type,
    cantBeBlank,
  } = otherProps;

  const inputLabel = __.isString(label) ? (
    <InputLabel {...props} />
  ) : (
    label({
      value: typing,
      name,
      error,
      htmlFor: name,
      capsLockOn,
      haveChanged: newIsNotOld,
    })
  );

  return (
    <div className="input-container">
      {inputLabel}

      <input
        ref={inputElement}
        value={typing}
        type={type}
        name={name}
        autoComplete={autoComplete}
        onChange={onChangeUpdate}
        onFocus={onFocus ? onFocus : __.noop()}
        onBlur={onBlur ? onBlur : __.noop()}
        // onPaste={onPaste ? onPaste : onPasteUpdate}
      />
      {touched && (
        <>
          <input
            type="text"
            readOnly
            value={oldValue.current}
            onChange={() => __.noop()}
          />
          <button type="button" onClick={() => undo()}>
            Undo
          </button>
        </>
      )}

      {error && (
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      )}
      {server && (
        <p aria-live="polite" input-error="">
          {server.msg}
        </p>
      )}
    </div>
  );
}
export { InputAdmin };
