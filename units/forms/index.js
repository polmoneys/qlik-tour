/**
 * React Forms component ð¹
 * @polmoneys  #2020#
 * version 1.0.1
 */

import React from "react";
import Form from "./form";
import CheckBox from "./checkbox";
import Input from "./input";
import { compactObject, unwrapXS } from "./utils";
import {
  InputLabel,
  CheckboxLabel,
  CheckboxIco,
  InputIcoLabel,
} from "./labels";

function Spacer() {
  return <br aria-hidden="true" />;
}

function Offscreen({ as = "p", children }) {
  const Tag = as;
  return <Tag className="offscreen">{children}</Tag>;
}

export {
  // comps
  Form,
  CheckBox,
  Input,
  // labels
  InputLabel,
  InputIcoLabel,
  CheckboxLabel,
  CheckboxIco,
  // <utils/>
  Spacer,
  Offscreen,
  // _.utils
  compactObject,
  unwrapXS,
};

export default Form;
