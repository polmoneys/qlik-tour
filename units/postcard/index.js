import React from "react";
import Shape3D from "../shape/3D";

const PALETTE = {
  pink: "rgb(244,190,212)",
  orange: "rgb(228,64,55)",
  purple: "rgb(99,91,199)",
  purpleDark: "rgb(20,19,65)",
  yellow: "rgb(255, 240, 155)",
};
const CityPostcard = ({ theme }) => {
  return (
    <div className="city flex main-center" aria-hidden="true">
      <div className="item">
        <Shape3D.Pentagon
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <HoodPostcard />
      <div style={{ transform: "translate(-290px, 100px)", zIndex: 2 }}>
        <NewHoodPostcard />
      </div>
      <div style={{ transform: "translate(-316px, 118px)", zIndex: 1 }}>
        <MixedHoodPostcard />
      </div>
      <div style={{ transform: "translate(-609px, 220px)", zIndex: 1 }}>
        <MixedHoodPostcard />
      </div>
    </div>
  );
};

const HoodPostcard = () => {
  return (
    <div className="cube-hood" aria-hidden="true">
      <div className="hood ">
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
    </div>
  );
};

const NewHoodPostcard = () => {
  return (
    <div className="condo-hood" aria-hidden="true">
      <div className="hood ">
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
    </div>
  );
};

const MixedHoodPostcard = () => {
  return (
    <div className="condo-hood" aria-hidden="true">
      <div className="hood ">
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
      <div className="hood ">
        <Shape3D.Cube
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
        <Shape3D.Obelisk
          shades={PALETTE.orange}
          borderColor={PALETTE.purpleDark}
          scaling={0.5}
          size={60}
        />
      </div>
    </div>
  );
};
const VenuePostcard = () => (
  <div className="flex row-gap" aria-hidden="true">
    <Shape3D.Terrain
      shades={PALETTE.orange}
      borderColor={PALETTE.purpleDark}
      scaling={0.8}
      size={60}
    />

    <Shape3D.Pentagon
      shades={PALETTE.orange}
      borderColor={PALETTE.purpleDark}
      scaling={0.8}
      size={60}
    />
    <Shape3D.Terrain
      shades={PALETTE.orange}
      borderColor={PALETTE.purpleDark}
      scaling={0.8}
      size={60}
    />
    <div style={{ transform: "translate(-140px,40px)" }}>
      <Shape3D.Terrain
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.8}
        size={60}
      />
    </div>

    <div style={{ transform: "translate(-60px,-10px)" }}>
      <Shape3D.Cube
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.8}
        size={60}
      />
    </div>

    <div style={{ transform: "translate(-60px,-10px)" }}>
      <Shape3D.Terrain
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.8}
        size={60}
      />
    </div>
  </div>
);

export { VenuePostcard, CityPostcard, HoodPostcard };

const All = () => (
  <>
    <div className="city traced">
      <Shape3D.SkyScraper
        variant={"pyramid"}
        offsetY={72}
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.5}
        size={60}
      />
      <Shape3D.SkyScraper
        variant={"tent"}
        offsetY={72}
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.5}
        size={60}
      />

      <Shape3D.Pyramid
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.5}
        size={60}
      />
      <Shape3D.Terrain
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.5}
        size={60}
      />

      <Shape3D.Tent
        shades={PALETTE.orange}
        borderColor={PALETTE.purpleDark}
        scaling={0.5}
        size={60}
      />
    </div>
  </>
);
