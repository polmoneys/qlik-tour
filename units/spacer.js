import React from "react";

const SPACER_SIZES = {
  xs: "8px",
  s: "12px",
  default: "16px",
  l: "32px",
  xl: "64px",
};

function Spacer({ size = "default" }) {
  const space = SPACER_SIZES[size];
  return (
    <div
      aria-hidden="true"
      className="flex self-grow"
      style={{ minHeight: space }}
      // vs. --spacer:`${space}px
    />
  );
}

export { Spacer };
