import { useEffect } from "react";

/**
 * 
 * 
 <a href="" data-tap />
 @ media (pointer: fine) {
  .item:hover {
    background-color: var(--backgroundColor);
    color: var(--color);
  }
}
.item[data-tap="active"] {
  background-color: var(--backgroundColor);
  color: var(--color);
  transition: background-color 0ms, color 0ms;
  transition-delay: 60ms;
}
.item.navItemActive {
  color: var(--color);
}


 */

const dataAttribute = "tap";
const tapActiveValue = "active";
const capture = false;

let isTouchDevice = false;

const getInteractiveEl = (event) => {
  try {
    return event
      .composedPath()
      .find((el) => el.dataset && el.dataset[dataAttribute] !== undefined);
  } catch (e) {
    return undefined;
  }
};

const removeClass = (event) => {
  const interactiveEl = getInteractiveEl(event);
  if (!interactiveEl) return;
  if (event.type === "click") {
    // keep the tap style on 1 tick later in case the UI blocks
    return setTimeout(() => {
      interactiveEl.dataset[dataAttribute] = "";
    });
  }
  interactiveEl.dataset[dataAttribute] = "";
};

// use "click" instead of "touchend" because it is triggered after touchend
// and we want the tap styles to stay on the element as long as possible
// (this makes a difference at least on later iOS versions)
const removeActiveClassEvents = ["touchmove", "touchcancel", "click"];

const onTouchStart = (event) => {
  if (!isTouchDevice) {
    isTouchDevice = true;
    // we only need to add these listeners if it's a touch device
    removeActiveClassEvents.forEach((event) =>
      document.body.addEventListener(event, removeClass, capture)
    );
  }
  const interactiveEl = getInteractiveEl(event);
  if (interactiveEl) {
    interactiveEl.dataset[dataAttribute] = tapActiveValue;
  }
};

function addTapListeners() {
  document.body.addEventListener("touchstart", onTouchStart, capture);
}

function removeTapListeners() {
  document.body.removeEventListener("touchstart", onTouchStart, capture);
  removeActiveClassEvents.forEach((event) =>
    document.body.removeEventListener(event, removeClass, capture)
  );
}

function useTapActive() {
  useEffect(() => {
    addTapListeners();
    return removeTapListeners;
  }, []);
}

export { useTapActive };
