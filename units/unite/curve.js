import React, { useState, useLayoutEffect, useEffect } from "react";
import { makePoints } from "./index";

let useIsomorphicEffect =
  typeof window === "undefined" ? useEffect : useLayoutEffect;

const Curve = ({ boundary, refs, theme }) => {
  const [pathD, setD] = useState(null);
  const [ready, setReady] = useState(false);

  useIsomorphicEffect(() => {
    if (boundary.current === null) return;
    let l = refs.filter((r) => r !== null).length;

    if (l === 0) return;
    if (pathD === null) drawLine();
  }, [boundary]);

  let drawLine = () => {
    setReady(false);

    makePoints(refs, boundary)
      .then((p) => makeCurvePath(p))
      .then((d) => {
        console.log(d);
        setD(d);
        setReady(true);
      });
  };

  useEffect(() => {
    /* eslint-disable */
    window.addEventListener("resize", drawLine);

    return () => {
      window.removeEventListener("resize", drawLine);
    };
  });

  if (!ready) return null;

  return (
    <svg
      style={{
        pointerEvents: "none",
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        overflow: "visible",
        zIndex: 1,
      }}
    >
      <path
        strokeLinejoin="round"
        strokeLinecap="round"
        fill="none"
        stroke={theme.strokeColor}
        strokeWidth={theme.strokeWidth}
        d={pathD}
      />
    </svg>
  );
};

export default Curve;

let smoothing = 0.2;

function makeCurvePath(points) {
  if (points.length === 0) return null;
  const d = points.reduce(function (acc, point, i, a) {
    return i === 0
      ? "M " + point[0] + "," + point[1]
      : acc + " " + bezierCommand(point, i, a);
  }, "");

  return new Promise((resolve) => resolve(d));
}

function line(pointA, pointB) {
  let lengthX = pointB[0] - pointA[0];
  let lengthY = pointB[1] - pointA[1];
  return {
    length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
    angle: Math.atan2(lengthY, lengthX),
  };
}

function controlPoint(current, previous, next, reverse) {
  let p = previous || current;
  let n = next || current;
  let o = line(p, n);
  let angle = o.angle + (reverse ? Math.PI : 0);
  let length = o.length * smoothing;
  let x = current[0] + Math.cos(angle) * length;
  let y = current[1] + Math.sin(angle) * length;
  return [x, y];
}
function bezierCommand(point, i, a) {
  let cps = controlPoint(a[i - 1], a[i - 2], point);
  let cpe = controlPoint(point, a[i - 1], a[i + 1], true);
  return (
    "C " +
    cps[0] +
    "," +
    cps[1] +
    " " +
    cpe[0] +
    "," +
    cpe[1] +
    " " +
    point[0] +
    "," +
    point[1]
  );
}
