import React, { useState, useLayoutEffect, useEffect } from "react";
import { makePoints } from "./index";

let useIsomorphicEffect =
  typeof window === "undefined" ? useEffect : useLayoutEffect;

const Line = ({ boundary, refs, theme }) => {
  const [pathD, setD] = useState(null);
  const [ready, setReady] = useState(false);

  useIsomorphicEffect(() => {
    let l = refs.filter((r) => r.current !== null);
    if (boundary.current === null || l === 0) return;
    console.log(boundary, l);
    if (pathD === null) drawLine();
  }, [boundary, refs]);

  let drawLine = () => {
    setReady(false);
    console.log(refs, boundary);
    makePoints(refs, boundary)
      .then((p) => makeD(p))
      .then((d) => {
        setD(d);
        setReady(true);
      });
  };

  useEffect(() => {
    /* eslint-disable */
    window.addEventListener("resize", drawLine);

    return () => {
      window.removeEventListener("resize", drawLine);
    };
  });

  if (!ready) return null;

  return (
    <svg
      style={{
        pointerEvents: "none",
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        overflow: "visible",
        zIndex: 1,
      }}
    >
      <path
        strokeLinecap="butt"
        fill="none"
        stroke={theme.strokeColor}
        strokeWidth={theme.strokeWidth}
        d={pathD}
      />
    </svg>
  );
};

export default Line;

function makeD(points) {
  if (points.length === 0) return null;
  const d = points.reduce(function (acc, point, i) {
    return i === 0 ? "M " + point[0] + "," + point[1] : acc + " " + point;
  }, "");
  return new Promise((resolveIt) => resolveIt(d));
}
