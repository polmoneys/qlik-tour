/**
 * React SVG line and bezier component 🚡 🚠
 * @polmoneys  #2020#
 * version 1.0.0
 */

import Curve from "./curve";
import Line from "./line";

export { Curve, Line };

function mapPoints(el, boundary) {
  const bounds = boundary.current.getBoundingClientRect();
  let rect;
  if ("current" in el) {
    rect = el.current.getBoundingClientRect();
  } else {
    rect = el.getBoundingClientRect();
  }
  const offsetWidth = Math.round(rect.width / 2);
  const offsetHeight = Math.round(rect.height / 2);
  return [
    Math.round(rect.left + offsetWidth - bounds.x),
    Math.round(rect.top + offsetHeight - bounds.y),
  ];
}

function makePoints(refs, boundary) {
  const points = refs.map((p) => mapPoints(p, boundary));
  return new Promise((resolveIt) => resolveIt(points));
}

export { makePoints };
