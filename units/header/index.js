import React, { useEffect, useState } from "react";
import { Action } from "../action";
import { SideBar } from "./sidebar";

import { Spacer } from "../spacer";

function Header() {
  return (
    <>
      <LandscapeNav />
      <SideBar>
        {({ close }) => (
          <>
            <div className="flex main-end">
              <Action
                theme="TRANSPARENT"
                onClick={close}
                className="h2 sans-theme"
              >
                &times;
              </Action>
            </div>
            <Action theme="TRANSPARENT" href="#summary" className="sans-theme">
              QLIK TOUR SUISSE
            </Action>
            <Action
              theme="TRANSPARENT"
              href="#experience"
              className="sans-theme"
            >
              EXPERIENCE
            </Action>
            <Action
              theme="TRANSPARENT"
              href="#practique"
              className="sans-theme"
            >
              PRACTIQUE
            </Action>

            <Action theme="TRANSPARENT" href="#contact" className="sans-theme">
              CONTACT
            </Action>
            <Action
              theme="TRANSPARENT"
              href="#experience"
              className="sans-theme -purple"
            >
              S'INSCRIRE
            </Action>
            <p className="font-s" style={{ cursor: "not-allowed" }}>
              {" "}
              EN | FR{" "}
            </p>
            <Spacer />
            <p>LOGO 1</p>
            <p>LOGO 2</p>
          </>
        )}
      </SideBar>
    </>
  );
}

const LandscapeNav = () => (
  <nav className="landscape flex-landscape main-end cross-center pxy">
    <div className="self-push-right flex row-gap -xl">
      <Action theme="TRANSPARENT" href="#" className="sans-theme">
        LOGO 1
      </Action>
      <Action theme="TRANSPARENT" href="#" className="sans-theme">
        LOGO 2
      </Action>
    </div>
    <Action theme="TRANSPARENT" href="#summary" className="sans-theme">
      QLIK TOUR SUISSE
    </Action>
    <Action theme="TRANSPARENT" href="#experience" className="sans-theme">
      EXPERIENCE
    </Action>
    <Action theme="TRANSPARENT" href="#practique" className="sans-theme">
      PRACTIQUE
    </Action>

    <Action theme="TRANSPARENT" href="#contact" className="sans-theme">
      CONTACT
    </Action>
    <Action
      theme="TRANSPARENT"
      href="#experience"
      className="sans-theme -orange traced "
    >
      S'INSCRIRE
    </Action>
    <p className="font-s" style={{ cursor: "not-allowed" }}>
      {" "}
      EN | FR{" "}
    </p>
  </nav>
);
export { Header };
