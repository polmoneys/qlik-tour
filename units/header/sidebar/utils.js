/* eslint-disable*/
function transitionEndPromise(element) {
  if (!element.current || element.current === null) {
    return new Promise((resolve) => {
      resolve();
    });
  } else {
    return new Promise((resolve) => {
      const node = element.current;
      node.addEventListener("transitionend", function f(event) {
        if (event.target !== node) return;
        node.removeEventListener("transitionend", f);
        resolve();
      });
    });
  }
}

function timeline() {
  return new Promise((resolve) => requestAnimationFrame(resolve));
}

function animate(element, stylz) {
  if (!element.current || element.current === null) {
    return transitionEndPromise(element).then((_) => timeline());
  } else {
    Object.assign(element.current.style, stylz);
    return transitionEndPromise(element).then((_) => timeline());
  }
}

export { animate, timeline };
