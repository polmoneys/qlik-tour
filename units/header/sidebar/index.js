/**
 * @polmoneys  #2020#
 */
import React, { useCallback, useRef, useState, useEffect } from "react";
import { animate, timeline } from "./utils";
import { Action } from "../../action";

function SideBar({ children, ...fillProps }) {
  const [status, setStatus] = useState(false);
  const [isAnimating, setProgress] = useState(false);
  const sidebarRef = useRef();
  const menuRef = useRef();

  const getToggleProps = () => ({
    "aria-haspopup": true,
    "aria-expanded": status,
  });

  const getSidebarProps = () => ({
    ref: sidebarRef,
    "aria-hidden": !status,
  });

  const toggle = useCallback(() => {
    if (isAnimating) return;
    setProgress(true);
    status ? close() : open();
  }, [isAnimating, status]);

  const open = () => {
    timeline()
      .then(() =>
        animate(menuRef, {
          transform: "translateX(200px)",
        })
      )
      .then(() =>
        animate(sidebarRef, {
          transform: "translateX(0)",
        })
      )
      .then(() => {
        setStatus((val) => !val);
        setProgress(false);
      });
  };

  const close = () => {
    timeline()
      .then(() =>
        animate(sidebarRef, {
          transform: "translateX(-300px)",
        })
      )
      .then(() =>
        animate(menuRef, {
          transform: "",
        })
      )
      .then(() => {
        setStatus((val) => !val);
        setProgress(false);
      });
  };

  useEffect(() => {
    window.addEventListener("keydown", keyboardHandle);
    return () => {
      window.removeEventListener("keydown", keyboardHandle);
    };
  });

  function keyboardHandle(e) {
    if (e.key !== "Escape") return;
    toggle();
  }

  const { fillOff = "var(--orange)", fillOn = "var(--pink)" } = fillProps;
  return (
    <>
      <header className="portrait flex main-end cross-center px">
        <button
          {...getToggleProps()}
          ref={menuRef}
          type="button"
          id="sidebar-button"
          onClick={toggle}
        >
          <svg viewBox={"0 -16 66 66"} width="66" height="66">
            <rect
              fill={status ? fillOn : fillOff}
              x="0"
              y="0"
              width="60"
              height="8"
            />
            <rect
              fill={status ? fillOn : fillOff}
              x="0"
              y="14"
              width="60"
              height="8"
            />
            <rect
              x="0"
              y="28"
              width="60"
              height="8"
              fill={status ? fillOn : fillOff}
            />
          </svg>
        </button>
        <Action
          theme="TRANSPARENT"
          href="#experience"
          className="traced self-push-left sans-theme -orange"
        >
          S'INSCRIRE
        </Action>
      </header>
      <nav className="portrait" ref={sidebarRef} {...getSidebarProps()}>
        {children &&
          children({
            close,
            getSidebarProps,
          })}
      </nav>
    </>
  );
}

export { SideBar };
