import React, { useEffect, useRef, useState } from "react";
import { getClasses } from "./index";

function Lazy(props) {
  const {
    url = "https://pbs.twimg.com/profile_images/1251216481267875842/5uW0lKVP_400x400.jpg",
    placeholder = "https://via.placeholder.com/300/000000",
  } = props;
  const [picSource, setPicSource] = useState(placeholder);
  const [observer, setElements, entries] = useLazy({
    threshold: 0.25,
    root: null,
  });
  const ref = useRef(null);
  useEffect(() => {
    if (ref.current === null) return;
    setElements([ref.current]);
  }, [setElements]);
  useEffect(() => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        const lazyImage = entry.target;
        // lazyImage.classList.remove("lazy");
        const newSrc = lazyImage.dataset.src;
        setPicSource(newSrc);
        observer.unobserve(lazyImage);
      }
    });
  }, [entries, observer]);
  const clss = getClasses(props.className, props.theme);
  return (
    <img
      ref={ref}
      data-src={url}
      className={clss}
      src={picSource}
      alt={props.alt || ""}
    />
  );
}
const useLazy = (options) => {
  const [elements, setElements] = useState([]);
  const [entries, setEntries] = useState([]);
  const observer = useRef(null);
  const { root, rootMargin, threshold } = options || {};
  useEffect(() => {
    if (elements.length) {
      // console.log("=>>>> CONNECTING");
      observer.current = new IntersectionObserver(
        (ioEntries) => {
          setEntries(ioEntries);
        },
        {
          threshold,
          root,
          rootMargin,
        }
      );
      elements.forEach((element) => {
        observer.current.observe(element);
      });
    }
    return () => {
      if (observer.current) {
        // console.log("=>>>> DISCONNECTING");
        observer.current.disconnect();
      }
    };
  }, [elements, root, rootMargin, threshold]);
  return [observer.current, setElements, entries];
};
export { Lazy };
