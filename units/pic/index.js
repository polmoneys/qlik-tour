/**
 * Pic
 * @polmoneys  #2020#
 * version 1.0.0
 */
import React from "react";
import { Lazy } from "./lazy";

function getClasses(className, theme) {
  return [
    className,
    theme === "PORTRAIT" && "pic-portrait",
    theme === "LANDSCAPE" && "pic-landscape",
    theme === "SQUARE" && "pic-square",
    theme === "AVATAR" && "pic-avatar",
    theme === "AVATARXL" && "pic-avatar-xl",
  ]
    .filter(Boolean)
    .join(" ");
}

function Pic({
  theme,
  className = false,
  url = "https://pbs.twimg.com/profile_images/1251216481267875842/5uW0lKVP_400x400.jpg",
  alt = "",
}) {
  if (!url.startsWith("https"))
    return (
      <p
        className={getClasses(
          "pic-placeholder flex col main-center cross-center",
          theme
        )}
      >
        invalid image url, should start with <b>https</b>
      </p>
    );

  const alternateText = alt || "";
  const clss = getClasses(className, theme);
  return <img className={clss} src={url} alt={alternateText} />;
}

Pic.Landscape = (props) => <Pic theme="LANDSCAPE" {...props} />;
Pic.Square = (props) => <Pic theme="SQUARE" {...props} />;
Pic.Portrait = (props) => <Pic theme="PORTRAIT" {...props} />;
Pic.Avatar = (props) => <Pic theme="AVATAR" {...props} />;
Pic.AvatarXL = (props) => <Pic theme="AVATARXL" {...props} />;
Pic.Lazy = (props) => <Lazy {...props} />;

export { getClasses, Pic };
