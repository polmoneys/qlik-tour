import React from "react";
import { Action } from "./index";

function ActionSpinner(props) {
  const {
    children = "",
    disabled = false,
    spinning = false,
    cb,
    theme = "DARK",
    size = 40,
    query = "data",
  } = props;

  const spinnerColor = () =>
    !spinning
      ? "transparent"
      : theme === "DARK"
      ? "rgba(255,255,255,.4)"
      : "rgba(12,12,12,.4)";

  const label = spinning ? (
    <span
      className="action-spinner"
      aria-label={`fetching ${query}`}
      style={{
        backgroundColor: spinnerColor(),
        width: `${size}px`,
        height: `${size}px`,
      }}
    />
  ) : (
    children
  );

  return (
    <Action
      theme={theme}
      className="sans-theme "
      type="button"
      disabled={disabled}
      onPress={() => cb()}
    >
      {label}
    </Action>
  );
}

export { ActionSpinner };
