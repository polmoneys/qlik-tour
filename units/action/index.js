/**
 * @polmoneys  #2020#
 * version 1.0.0
 *
 */
import React, { useRef } from "react";
import Link from "next/link";
import ActionCard from "./action-card";
import { ActionSpinner } from "./action-spinner";
import TYPES from "./types";
import { useButton } from "@react-aria/button";

const Action = (props) => {
  const { className, to, href, target, theme, ...rest } = props;
  let ref = useRef();
  let { buttonProps } = useButton(props, ref);

  const Tag = to ? Link : href ? "a" : "button";
  const rel = getRel(props);
  const type = Tag === "button" ? "button" : undefined;
  const clss = getClasses(className, theme);

  const { children, ico = false, ...events } = rest;
  const goodProps = { to, href, events };

  return (
    <Tag
      {...goodProps}
      ref={ref}
      rel={rel}
      {...(Tag === "button" && { ...buttonProps })}
      className={clss}
      type={type}
    >
      {children}
      {target === "_blank" && (
        <small style={{ paddingLeft: "8px", fontWeight: "bold" }}>EXT</small>
      )}
      {ico && ico}
    </Tag>
  );
};

export { Action, ActionCard, ActionSpinner };

Action.propTypes = TYPES.propTypes;
Action.defaultProps = TYPES.defaultProps;

function getRel(props) {
  if (props.target === "_blank") {
    return (props.rel || "") + "noopener noreferrer";
  }

  return props.rel;
}

function getClasses(className, theme) {
  return [
    "action",
    className,
    theme === "ACCENT" && "action-accent",
    theme === "DARK" && "action-dark",
    theme === "LIGHT" && "action-light",
    theme === "TRANSPARENT" && "action-transparent",
  ]
    .filter(Boolean)
    .join(" ");
}

/*   
    Notes:
    ------
      
      https://gist.github.com/polmoneys/cb42aea8f677c1e8a835b7992f8e6c1e#file-usetaptoggle-js

      role={disabled ? undefined : "button"}
      tabIndex={disabled ? undefined : 0}
      
      onKeyUp={handleKeyUp}
      
      const TAB_KEY_CODE = 9;

      const handleKeyUp = e => {
          if (target === "blank" && e.keyCode === TAB_KEY_CODE && href) {
            setEnableFocusStyles(true);
          }
        }; 

  */
