/**
 * @polmoneys  #2020#
 */

import React, { cloneElement } from "react";
import { Action } from "./index";

function CardAction(props) {
  let defaultClss = ["self-grow", "flex", "col", "main-start", "cross-center"];
  const actionClss = [...defaultClss, props.actionClassName]
    .filter(Boolean)
    .join(" ");
  return (
    <Action {...props} className={actionClss} theme="transparent">
      {props.children}
    </Action>
  );
}

function Card(props) {
  const clss = getClasses(props.className, props.theme);

  if (!props.extra)
    return (
      <div className={clss}>
        <CardAction {...props} />
      </div>
    );

  const typeOfExtra = props.extraTo
    ? { to: props.extraTo }
    : props.extraOnClick
    ? { onClick: props.extraOnClick }
    : { href: props.extraHref };

  const wrap = (props) => (
    <div className={clss}>
      <CardAction {...typeOfExtra}>{props.extra}</CardAction>
      <CardAction {...props} />
    </div>
  );
  return <>{cloneElement(wrap(props))}</>;
}

function getClasses(className, theme) {
  return [
    className,
    "action-card",
    "flex",
    "col",
    theme === "DARK"
      ? "action-card-dark"
      : theme === "LIGHT"
      ? "action-card-light"
      : "action-card-transparent",
  ]
    .filter(Boolean)
    .join(" ");
}

export default Card;
