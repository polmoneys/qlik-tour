import React from "react";
import { Ico } from "../icon";

function Footer({ children }) {
  return (
    <footer className="pxy">
      {children}
      <div className="flex-landscape cross-start">
        <div className="flex self self-push-right self-stretch  main-start cross-center row-gap">
          <Ico color="var(--orange)" />
          <p className="h3">Nous suivre </p>
        </div>
        <nav className="flex self row-gap -xl main-end cross-end">
          <a href="/mentions">Mentions legales</a>
          <a href="/privacy">Privacy policy</a>
        </nav>
      </div>
    </footer>
  );
}

export { Footer };
