function draw(v, c, angle, offset, r1) {
  let d = [];
  for (let i = 0; i < v; i++) {
    let a = angle * i - offset;
    let line = [i === 0 ? "M" : "L", num(rx(r1, a, c)), num(ry(r1, a, c))].join(
      " "
    );
    d.push(line);
  }

  return [d.join(" ")];
}

let rad = function (a) {
  return (Math.PI * a) / 180;
};

let rx = function (r, a, c) {
  return c + r * Math.cos(rad(a));
};

let ry = function (r, a, c) {
  return c + r * Math.sin(rad(a));
};

let num = function (n) {
  return n < 0.0000001 ? 0 : n;
};

export default {
  draw,
};
