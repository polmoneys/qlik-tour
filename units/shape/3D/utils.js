const round_to = (n, precision) =>
  Math.round(n * 10 ** precision) / 10 ** precision;

const unwrap_arr = (arg) => (Array.isArray(arg) ? arg[0] : arg);
const sleep = (ms = 1000) => new Promise((res) => setTimeout(res, ms));
const scale = (num, in_min, in_max, out_min, out_max) => {
  return ((num - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
};
const calc_min = (arr) =>
  arr
    .map((x) => x.value)
    .reduce((previous, current) => {
      return previous < current ? previous : current;
    });

const calc_max = (arr) =>
  arr
    .map((x) => x.value)
    .reduce((previous, current) => {
      return previous > current ? previous : current;
    });

const calc_item_height = (v, min, max, chart_height, topSpace) =>
  round_to(
    scale(
      v,
      min,
      max,
      0,
      v < topSpace ? chart_height : chart_height - topSpace
    ),
    0
  );

const calc_item_width = (chart_width, chunk) =>
  round_to(chart_width / chunk, 0);

const calc_color = (shades, i) =>
  Array.isArray(shades)
    ? RGB_Linear_Blend(i / 10, shades[0], shades[1])
    : RGB_Linear_Shade((i * 16) / 100, shades);

export const RGB_Linear_Blend = (p, c0, c1) => {
  var i = parseInt,
    r = Math.round,
    P = 1 - p,
    [a, b, c, d] = c0.split(","),
    [e, f, g, h] = c1.split(","),
    x = d || h,
    d = x
      ? "," +
        (!d
          ? h
          : !h
          ? d
          : r((parseFloat(d) * P + parseFloat(h) * p) * 1000) / 1000 + ")")
      : ")";
  return (
    "rgb" +
    (x ? "a(" : "(") +
    r(
      i(a[3] == "a" ? a.slice(5) : a.slice(4)) * P +
        i(e[3] == "a" ? e.slice(5) : e.slice(4)) * p
    ) +
    "," +
    r(i(b) * P + i(f) * p) +
    "," +
    r(i(c) * P + i(g) * p) +
    d
  );
};

export const RGB_Linear_Shade = (p, c) => {
  var i = parseInt,
    r = Math.round,
    [a, b, c, d] = c.split(","),
    P = p < 0,
    t = P ? 0 : 255 * p,
    P = P ? 1 + p : 1 - p;
  return (
    "rgb" +
    (d ? "a(" : "(") +
    r(i(a[3] == "a" ? a.slice(5) : a.slice(4)) * P + t) +
    "," +
    r(i(b) * P + t) +
    "," +
    r(i(c) * P + t) +
    (d ? "," + d : ")")
  );
};

export default {
  calc_color,
  calc_item_height,
  calc_item_width,
  calc_min,
  calc_max,
  scale,
};
