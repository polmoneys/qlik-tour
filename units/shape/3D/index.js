/**
 * React Shape component 🔺 🔴 ◾️ 🔸
 * @polmoneys  #2020#
 * version 1.0.0
 */

import React, { memo } from "react";
import PropTypes from "prop-types";
import { default as __ } from "./utils";
import { colors } from "./styles";
import Cube from "./cube";
import Composed from "./composed";
import Penta from "./penta";
import Pyramid from "./pyramid";
import Obelisk from "./obelisk";
import Billboard from "./billboard";
import Terrain from "./terrain";
import Tent from "./tent";

const Base = (props) => {
  const {
    a11yTitle,
    boundaryX,
    boundaryY,
    borderWidth,
    borderColor,
    cb,
    chartBg,
    round = false,
    scaling,
    shades = [colors.accent, colors.accent_pro],
    size,
    variant,
  } = props;

  const computedProps = (itemProps) => ({
    onClick: cb,
    round,
    scaling,
    shades,
    size,
    stroke_width: borderWidth,
    stroke: borderColor,
    ...itemProps,
  });
  return (
    <svg
      aria-label={a11yTitle ? a11yTitle : `${a11yTitle}, a ${variant}`}
      style={{
        background: chartBg,
        overflow: "visible",
      }}
      viewBox={`${boundaryX} ${boundaryY} ${size} ${size}`}
      width={size}
      height={size}
    >
      {
        {
          cube: <Cube {...computedProps()} />,
          pyramid: <Pyramid {...computedProps()} />,
          pentagon: <Penta {...computedProps()} />,
          tent: <Tent {...computedProps()} />,
          obelisk: <Obelisk {...computedProps()} />,
          billboard: <Billboard {...computedProps()} />,
          terrain: <Terrain {...computedProps()} />,
        }[variant]
      }
    </svg>
  );
};

const ifTrueAvoidReRender = (prevProps, nextProps) => {
  return prevProps.variant === nextProps.variant;
};

const Shape3D = memo((props) => <Base {...props} />, ifTrueAvoidReRender);

Shape3D.Cube = (props) => <Base variant="cube" {...props} />;
Shape3D.Pyramid = (props) => <Base variant="pyramid" {...props} />;
Shape3D.Tent = (props) => <Base variant="tent" {...props} />;
Shape3D.Pentagon = (props) => <Base variant="pentagon" {...props} />;
Shape3D.SkyScraper = (props) => <Composed {...props} />;
Shape3D.Obelisk = (props) => <Base variant="obelisk" {...props} />;
Shape3D.Billboard = (props) => <Base variant="billboard" {...props} />;
Shape3D.Terrain = (props) => <Base variant="terrain" {...props} />;
export default Shape3D;

Base.propTypes = {
  a11yTitle: PropTypes.string,
  boundaryX: PropTypes.number,
  boundaryY: PropTypes.number,
  chartBg: PropTypes.string,
  cb: PropTypes.func,
  /**
   * only for <Shape3D.SkyScraper/>
   */
  elevation: PropTypes.number,
  shades: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.string,
  ]),
  borderWidth: PropTypes.number,
  borderColor: PropTypes.string,
  size: PropTypes.number,
  scaling: PropTypes.number,
  variant: PropTypes.oneOf([
    "billboard",
    "obelisk",
    "compose",
    "cube",
    "pyramid",
    "pentagon",
    "skyscraper",
    "tent",
    "terrain",
  ]),
};

Base.defaultProps = {
  boundaryX: 0,
  boundaryY: 0,
  cb: () => {},
  elevation: 30,
  scaling: 1,
  size: 100,
  chartBg: "transparent",
  shades: colors.accent,
  a11yTitle: "A 3D shape",
  borderWidth: 1,
  borderColor: "transparent",
};
