import React from "react";
import { RGB_Linear_Shade } from "./utils";

//face trapezi
// `M ${origin.x + vertex.x },${origin.y + vertex.yy}
//       L
//       ${origin.x + vertex.x}, ${origin.y + vertex.yyyy}
//       ${origin.x + vertex.xx}, ${origin.y + vertex.yyy}
//       ${origin.x + xx}, ${origin.y + y}
//       ${origin.x + vertex.x},${origin.y + vertex.yy}
//     z
//     `

const Billboard = ({
  onClick,
  scaling,
  shades,
  size,
  stroke_width,
  stroke,
}) => {
  const origin = {
    x: 0,
    y: size - 115 * scaling,
  };

  const vertex = {
    y: 9 * scaling,
    yy: 26 * scaling,
    yyy: 87 * scaling,
    yyyy: 110 * scaling,
    x: 30 * scaling,
    xx: 100 * scaling,
  };

  const faces = [
    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x}, ${origin.y + vertex.yyy}
        ${origin.x + vertex.x}, ${origin.y + vertex.yyyy}
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x},${origin.y + vertex.y}
      z`,

    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x + vertex.xx}, ${origin.y + vertex.y}
        ${origin.x + vertex.x}, ${origin.y}
       ${origin.x},${origin.y + vertex.y}
      z`,
    `M ${origin.x + vertex.x},${origin.y + vertex.yy} 
        L 
        ${origin.x + vertex.x}, ${origin.y + vertex.yyyy}
        ${origin.x + vertex.xx}, ${origin.y + vertex.yyy}
        ${origin.x + vertex.xx}, ${origin.y + vertex.y}
        ${origin.x + vertex.x},${origin.y + vertex.yy}
      z
      `,
  ];

  return (
    <g
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
      style={{
        cursor: "pointer",
      }}
    >
      {faces.map((f, i) => (
        <path
          key={`${i}-face`}
          d={f}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Billboard;
