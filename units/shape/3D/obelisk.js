import React from "react";
import { RGB_Linear_Shade } from "./utils";

const Obelisk = ({ onClick, scaling, shades, size, stroke_width, stroke }) => {
  const origin = {
    x: 0,
    y: size - 115 * scaling,
  };

  const vertex = {
    y: 29 * scaling,
    yy: 57 * scaling,
    yyy: 87 * scaling,
    yyyy: 115 * scaling,
    x: 50 * scaling,
    xx: 100 * scaling,
  };

  let x = 30 * scaling;
  let xx = 80 * scaling;
  let y = 42 * scaling;
  let yy = 13 * scaling;
  let yyy = 100 * scaling;

  const faces = [
    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x}, ${origin.y + vertex.yyy}
        ${origin.x + vertex.x}, ${origin.y + vertex.yyyy}
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x},${origin.y + vertex.y}
      z`,
    // top
    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x + xx}, ${origin.y + y}
        ${origin.x + x}, ${origin.y + yy}
       ${origin.x},${origin.y + vertex.y}
      z`,
    // right
    `M ${origin.x + vertex.x},${origin.y + vertex.yy} 
        L 
        ${origin.x + vertex.x}, ${origin.y + vertex.yyyy}
        ${origin.x + xx}, ${origin.y + yyy}
        ${origin.x + xx}, ${origin.y + y}
        ${origin.x + vertex.x},${origin.y + vertex.yy}
      z
      `,
  ];
  return (
    <g
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
      style={{
        cursor: "pointer",
      }}
    >
      {faces.map((f, i) => (
        <path
          key={`${i}-face`}
          d={f}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Obelisk;
