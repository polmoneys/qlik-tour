import React from "react";
import { RGB_Linear_Shade } from "./utils";

const Cube = ({ onClick, scaling, shades, size, stroke_width, stroke }) => {
  const origin = {
    x: 0,
    y: size * scaling,
  };

  const vertex = {
    y: 29 * scaling,
    yy: 57 * scaling,
    yyy: 87 * scaling,
    yyyy: 115 * scaling,
    x: 50 * scaling,
    xx: 100 * scaling,
  };

  const faces = [
    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x + vertex.xx}, ${origin.y + vertex.y}
        ${origin.x + vertex.x}, ${origin.y}
       ${origin.x},${origin.y + vertex.y}
      z`,
  ];

  return (
    <g
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
      style={{
        cursor: "pointer",
      }}
    >
      {faces.map((f, i) => (
        <path
          key={`${i}-face`}
          d={f}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Cube;
