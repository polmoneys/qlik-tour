import React from "react";
import { RGB_Linear_Shade } from "./utils";

const Penta = ({ onClick, scaling, shades, size, stroke_width, stroke }) => {
  const origin = {
    x: 0,
    y: size - 112 * scaling,
  };

  const vertex = {
    y: 19 * scaling,
    yy: 39 * scaling,
    yyy: 93 * scaling,
    yyyy: 112 * scaling,
    x: 25 * scaling,
    xx: 73 * scaling,
    xxx: 98 * scaling,
  };

  const faces = [
    `M ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x + vertex.x}, ${origin.y}
        ${origin.x + vertex.xx}, ${origin.y}
        ${origin.x + vertex.xxx}, ${origin.y + vertex.y}
        ${origin.x + vertex.xx}, ${origin.y + vertex.yy}
        ${origin.x + vertex.x}, ${origin.y + vertex.yy}
        ${origin.x},${origin.y + vertex.y}
        z
      `,
    `M  ${origin.x},${origin.y + vertex.y} 
        L 
        ${origin.x},${origin.y + vertex.yyy}
        ${origin.x + vertex.x},${origin.y + vertex.yyyy}
        ${origin.x + vertex.x},${origin.y + vertex.yy} 
        z`,
    `M  ${origin.x + vertex.x},${origin.y + vertex.yy} 
        L 
        ${origin.x + vertex.x},${origin.y + vertex.yyyy}
        ${origin.x + vertex.xx},${origin.y + vertex.yyyy}
        ${origin.x + vertex.xx},${origin.y + vertex.yy}
        z`,
    `M ${origin.x + vertex.xx},${origin.y + vertex.yy} 
        L 
        ${origin.x + vertex.xxx},${origin.y + vertex.y}
        ${origin.x + vertex.xxx},${origin.y + vertex.yyy}
        ${origin.x + vertex.xx},${origin.y + vertex.yyyy}
        z`,
  ];

  return (
    <g
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
      style={{
        cursor: "pointer",
      }}
    >
      {faces.map((f, i) => (
        <path
          key={`${i}-face`}
          d={f}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Penta;
