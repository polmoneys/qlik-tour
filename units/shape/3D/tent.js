import React from "react";
import { RGB_Linear_Shade } from "./utils";

const Tent = ({ onClick, scaling, shades, size, stroke_width, stroke }) => {
  const origin = {
    x: 0,
    y: size - 98 * scaling,
  };

  const square = {
    x: 24 * scaling,
    xx: 50 * scaling,
    xxx: 70 * scaling,
    xxxx: 100 * scaling,
    y: 26 * scaling,
    yy: 60 * scaling,
    yyy: 90 * scaling,
  };

  const faces = [
    `M ${origin.x + square.x},${origin.y} 
      L 
      ${origin.x}, ${origin.y + square.yy}
      ${square.xx}, ${origin.y + square.yyy}
      ${square.xxx}, ${origin.y + square.y}
      z
      `,
    `
      M ${square.xx}, ${origin.y + square.yyy}
       ${square.xxxx}, ${origin.y + square.yy}
      ${square.xxx}, ${origin.y + square.y}
      z
      `,
  ];

  return (
    <g
      style={{
        cursor: "pointer",
      }}
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
    >
      {faces.map((f, i) => (
        <path
          d={f}
          key={`${i}-face-tent`}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Tent;
