import React from "react";
import { RGB_Linear_Shade } from "./utils";

const Pyramid = ({ onClick, scaling, shades, size, stroke_width, stroke }) => {
  const origin = {
    x: 0,
    y: size - 96 * scaling,
  };

  const vertex = {
    y: 59 * scaling,
    yy: 87 * scaling,
    x: 50 * scaling,
    xx: 100 * scaling,
  };

  const faces = [
    `M ${origin.x + vertex.x},${origin.y} 
      L 
      ${origin.x}, ${origin.y + vertex.y}
      ${origin.x + vertex.x}, ${origin.y + vertex.yy}
      z
      `,
    `
       M ${origin.x + vertex.x},${origin.y} 
       ${origin.x + vertex.x}, ${origin.y + vertex.yy}
      ${origin.x + vertex.xx},${origin.y + vertex.y}
      z
      `,
  ];

  return (
    <g
      style={{
        cursor: "pointer",
      }}
      aria-hidden="true"
      stroke={stroke}
      strokeWidth={stroke_width}
      onClick={onClick}
    >
      {faces.map((f, i) => (
        <path
          d={f}
          key={`${i}-face-pyramid`}
          fill={RGB_Linear_Shade((i * 16) / 100, shades)}
        />
      ))}
    </g>
  );
};
export default Pyramid;
