import React, { useState } from "react";
import Shape3D from "./index";

let Composed = ({
  a11yTitle = "Hello",
  cb = () => ({}),
  chartBg,
  variant = "pyramid",
  offsetY = 120,
  offsetX = 0,
  contained = false,
  ...moreProps
}) => {
  let { size, scaling = 0.8 } = moreProps;

  let stylesComposable = {
    position: "relative",
    overflow: contained ? "hidden" : "visible",
    width: "100%",
    maxWidth: `${size}px`,
    minHeight: `${size}px`,
    backgroundColor: chartBg,
  };
  let stylesComposed = {
    position: "absolute",
    width: "100%",
    maxWidth: `${size}px`,
    minHeight: `${size}px`,
    top: "-8px",
  };

  return (
    <>
      <div
        style={stylesComposable}
        aria-label={a11yTitle ? a11yTitle : `${a11yTitle}, a ${variant}`}
        onClick={() => cb()}
      >
        <div
          style={{
            ...stylesComposed,
            top: `-${offsetY * scaling}px`,
            zIndex: 2,
          }}
        >
          {variant === "pyramid" ? (
            <Shape3D.Pyramid {...moreProps} />
          ) : (
            <Shape3D.Tent {...moreProps} />
          )}
        </div>
        <div style={stylesComposed}>
          <Shape3D.Cube {...moreProps} />
        </div>
      </div>
    </>
  );
};

export default Composed;
