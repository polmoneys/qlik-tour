export const colors = {
  accent: "rgb(247, 8, 72)",
  accent_alpha: "rgba(247, 8, 72, 0.1)",
  accent_pro: "rgb(135, 11, 243)",
  accent_pro_alpha: "rgba(135, 11, 243, 0.1)",
  chart_bg: "rgba(0, 0, 0, 0.1)",
  chart_tips: "rgba(0, 0, 0, 0.4)",
  black_alpha: "rgba(0, 0, 0, 0.8)",
  pristine: "#ffffff",
  pristine_alpha: "rgba(255, 255, 255, .5)",
  transparent: "transparent",
  grey: "rgb(122,122,122)",
  green: "rgb(60,196,124)",
};

const scale = {
  xs: "10px",
  s: "12px",
  r: "15px",
  l: "18px",
  xl: "24px",
  xxl: "66px",
};

const spaces = {
  xs: "4px",
  s: "6px",
  r: "8px",
  l: "12px",
  xl: "20px",
};

const layers = {
  base: 1,
  mid: 2,
  top: 3,
  max: 10,
};

const g = (obj) => (k) => obj[k];
const capitalize = ([first, ...rest]) =>
  first ? first.toUpperCase() + rest.join("") : "";

const font = (k) => ({ fontSize: g(scale)(k) });
const elevation = (k) => ({ zIndex: g(layers)(k) });
const spacing = (domain) => (k) => ({ [domain]: g(spaces)(k) });
const spacing_axis = (axis) => (domain) => (k) => {
  if (axis === "y") {
    return {
      [`${domain}Top`]: g(spaces)(k),
      [`${domain}Bottom`]: g(spaces)(k),
    };
  }
  return {
    [`${domain}Left`]: g(spaces)(k),
    [`${domain}Right`]: g(spaces)(k),
  };
};
const spacing_direction = (direction) => (domain) => (k) => ({
  [`${domain}${capitalize(direction)}`]: g(spaces)(k),
});

const padded = (k) => spacing("padding")(k);
const paddedY = (k) => spacing_axis("y")("padding")(k);
const margin = (k) => spacing("margin")(k);
const marginRight = (k) => spacing_direction("right")("margin")(k);
const flex_box = (direction) => (justify) => (align) => ({
  display: "flex",
  flexDirection: direction,
  justifyContent: justify,
  alignItems: align,
});
const box = {
  col_center: (align) => flex_box("column")("center")(align),
  row_center: (align) => flex_box("row")("center")(align),
  col_start: (align) => flex_box("column")("flex-start")(align),
  row_start: (align) => flex_box("row")("flex-start")(align),
  row_between: (align) => flex_box("row")("space-between")(align),
  col_between: (align) => flex_box("column")("space-between")(align),
  stretch: {
    display: "flex",
    alignSelf: "stretch",
  },
  fill: {
    width: "100%",
    flex: "1 1 auto!important",
  },
};

const txt = {
  bold: { fontWeight: 600 },
  numbers: { fontVariantNumeric: "tabular-nums" },
  truncate_sx: (w) => ({
    display: "block",
    textOverflow: "ellipsis",
    overflow: "hidden",
    width: `${w}px`,
  }),
  uppercase: {
    fontSize: "0.9rem",
    letterSpacing: "0.05rem",
    textTransform: "uppercase",
    fontWeight: 500,
  },
  call_to_action: {
    color: colors.accent,
    fontSize: font("xl"),
    textAlign: "center",
  },
};

const pinned = {
  position: "absolute",
  top: " 50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  zIndex: layers.max,
};

export default {
  ...box,
  ...txt,
  elevation,
  marginRight,
  padded,
  paddedY,
  pinned,
  font,
};
