/**
 * React Shape component 🔺 🔴 ◾️ 🔸
 * @polmoneys  #2020#
 * version 1.0.0
 */

import React from "react";
import PropTypes from "prop-types";
import { default as __ } from "./utils";

let absoluteCenterStyles = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
};

function Shape({ sides, children, ...theme }) {
  const { transforms, size, fill, highlight, ...propsLeft } = theme;

  /* eslint-disable no-undef */
  if (process.env.NODE_ENV === "development") {
    if (sides < 3) {
      console.warn(
        "We need at least 3 sides to create a shape so sides will be clamped to 3"
      );
    }
    if (sides > 60) {
      console.warn("Too many sides, shape will be clamped to 60");
    }
  }
  /* eslint-enable */

  const clampedSize = sides < 3 ? 3 : sides > 60 ? 60 : sides;
  const center = size / 2;
  const r1 = (1 * size) / 2;
  const angle = 360 / clampedSize;
  const offset = 90;
  const box = [0, 0, size, size];
  const path = __.draw(clampedSize, center, angle, offset, r1);

  return (
    <div
      style={{
        position: "relative",
        display: "inline-flex",
        maxWidth: `${size}px`,
        transform: transforms,
      }}
      {...propsLeft}
    >
      <svg
        viewBox={box}
        width={size}
        height={size}
        fill={fill}
        style={{
          filter: highlight
            ? `drop-shadow( 1px -2px 1px ${highlight})`
            : "none",
        }}
      >
        <path d={path} />
      </svg>

      {children && (
        <div
          style={{
            ...absoluteCenterStyles,
            zIndex: 1,
          }}
        >
          {children}
        </div>
      )}
    </div>
  );
}

Shape.Circle = (props) => <Shape sides={25} {...props} />;
Shape.Triangle = (props) => <Shape sides={3} {...props} />;
Shape.Square = (props) => <Shape sides={4} {...props} />;

const avoidRerenderIf = (prevProps, nextProps) => {
  return prevProps.sides === nextProps.sides;
};
Shape.Freeze = React.memo((props) => <Shape {...props} />, avoidRerenderIf);

Shape.propTypes = {
  sides: PropTypes.number,
  fill: PropTypes.string,
  transforms: PropTypes.string,
  size: PropTypes.number,
  highlight: PropTypes.any,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

Shape.defaultProps = {
  sides: 3,
  size: 69,
  fill: "currentColor",
  transforms: null,
  children: null,
  highlight: false,
};

export default Shape;
