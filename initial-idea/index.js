import React from "react";

const MINUTES_IN_DAY = 24 * 60;
const BITCOIN_PER_DAY = 6000000000; // US BILLIONS
const BLINKS_PER_DAY = 10000;
const YOUTUBE_USERS_PER_DAY = 149000000;
const IG_POSTS_PER_DAY = 95000000;
const TIKTOK_TIME_PER_DAY = 52; // MINUTES/USER
const HEARTBEATS_PER_DAY = 103680;
const GAME_TIME = 90; // MINUTES SOCCER
const HALTANDCATCHFIRE_MARATHON_LENGTH = 40; // HOURS

const roundTo = (str, roundDecimals) =>
  Math.round(str * 10 ** roundDecimals) / 10 ** roundDecimals;

const toLocale = (num) =>
  num.toLocaleString(
    undefined, // leave undefined to use the browser's locale,
    // or use a string like 'en-US' to override it.
    { minimumFractionDigits: 0 }
  );

const ChartCard = ({ children }) => (
  <div className="card _accent flex col main-between cross-center">
    {children}
  </div>
);

const Charts = (props) => {
  const {
    days,
    // hours, minutes, seconds, completed
  } = props;

  return (
    <div className="flex grid col-gap">
      <ChartCard>
        <p className="h4">in the next {days} days</p>
        <p className="font-bold center-transforms">
          ${toLocale(BITCOIN_PER_DAY * days)}
        </p>
        <p className="h4">in Bitcoin transactions</p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days you will blink</p>
        <p className="font-bold center-transforms">
          {toLocale(BLINKS_PER_DAY * days)}
        </p>
        <p className="h4">times </p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days</p>
        <p className="font-bold center-transforms ">
          {toLocale(YOUTUBE_USERS_PER_DAY * days)}
        </p>
        <p className="h4">users will visit Youtube </p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days your heart will beat</p>
        <p className="font-bold center-transforms">
          {toLocale(HEARTBEATS_PER_DAY * days)}
        </p>
        <p className="h4">times (more if in love) </p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days you can re-watch</p>
        <p className="font-bold center-transforms">
          {roundTo((days * 24) / HALTANDCATCHFIRE_MARATHON_LENGTH, 0)}
        </p>
        <p className="h4">times Halt and Catch Fire on repeat </p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days</p>
        <p className="font-bold center-transforms">0</p>
        <p className="h4">
          {" "}
          <span role="img" className="h3" aria-label="dinosaur">
            🦖
          </span>{" "}
          will be born :({" "}
        </p>
      </ChartCard>

      <ChartCard>
        <p className="h4">in the next {days} days</p>
        <p className="font-bold center-transforms">
          {toLocale(IG_POSTS_PER_DAY * days)}
        </p>
        <p className="h4">selfies will be posted on IG</p>
      </ChartCard>
      <ChartCard>
        <p className="h4">in the next {days} days a Tik Tok user will spend</p>
        <p className="font-bold center-transforms">
          {toLocale(TIKTOK_TIME_PER_DAY * (days * 24))}
        </p>
        <p className="h4">minutes on average</p>
      </ChartCard>

      {/*       <ChartCard>
        <p className="h4">in the next {days} days</p>
        <p className="font-bold center-transforms">
          {roundTo((days * MINUTES_IN_DAY) / GAME_TIME, 2)}
        </p>
        <p className="h4">soccer games could be played </p>
      </ChartCard> */}
    </div>
  );
};

export { Charts };
