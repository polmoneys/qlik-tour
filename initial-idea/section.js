import React from "react";

function Section({ children, id, ...themeProps }) {
  return (
    <section id={id} className={themeSection(themeProps)}>
      <div>{children}</div>
    </section>
  );
}

function themeSection(props) {
  const { theme = "pink", size = "l" } = props;
  return [
    "pxy flex col main-center cross-center",
    `section-${size}`,
    theme === "pink" && "_pink",
    theme === "orange" && "_orange",
    theme === "yellow" && "_yellow",
    theme === "purple" && "_purple",
    theme === "purple-dark" && "_purple-dark -yellow",
  ]
    .filter(Boolean)
    .join(" ");
}

Section.XS = (props) => <Section size="xs" {...props} />;
Section.S = (props) => <Section size="s" {...props} />;
Section.L = (props) => <Section size="l" {...props} />;
Section.XL = (props) => <Section size="xl" {...props} />;

export { Section };
