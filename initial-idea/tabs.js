import React, { useState } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import "@reach/tabs/styles.css";

// EXAMPLE OF DATA-DRIVEN TABS, MAY COME IN HANDY IF MULTILINGUAL

function DataTabs({ data }) {
  return (
    <Tabs>
      <TabList>
        {data.map((tab, index) => (
          <Tab key={index}>{tab.label}</Tab>
        ))}
      </TabList>
      <TabPanels>
        {data.map((tab, index) => (
          <TabPanel key={index}>{tab.content}</TabPanel>
        ))}
      </TabPanels>
    </Tabs>
  );
}
// now if you have an array of data...
const tabData = [
  { label: "Taco", content: "Perhaps the greatest dish ever invented." },
  {
    label: "Burrito",
    content:
      "Perhaps the greatest dish ever invented but bigger and with rice.",
  },
];

function TabSectionDynamic({ tabData }) {
  // you can just pass it in:
  return <DataTabs data={tabData} />;
}

export { TabSectionDynamic };
