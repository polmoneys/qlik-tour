import React from "react";
import { zeroPad } from "react-countdown";
import { PostCountdown } from "./post";

const DisplayCard = ({ children, isFeatured = false }) => {
  const clss = isFeatured
    ? "countdown-chars self flex col cross-center -orange"
    : "countdown-chars self flex col cross-center";
  return <div className={clss}> {children} </div>;
};

const DisplayCardFeatured = (props) => <DisplayCard isFeatured {...props} />;

const Display = ({ days, hours, minutes, seconds, completed }) => {
  if (completed) return <PostCountdown />;
  return (
    <div className="flex grid section-s" aria-hidden="true">
      <DisplayCard>
        <p className="h0"> {zeroPad(days)}</p>
        <p> Days</p>
      </DisplayCard>
      <DisplayCard>
        <p className="h0"> {zeroPad(hours)}</p>
        <p> Hours</p>
      </DisplayCard>
      <DisplayCard>
        <p className="h0"> {zeroPad(minutes)}</p>
        <p> Minutes</p>
      </DisplayCard>
      <DisplayCardFeatured>
        <p className="h0"> {zeroPad(seconds)}</p>
        <p> Seconds</p>
      </DisplayCardFeatured>
    </div>
  );
};
export { Display };
