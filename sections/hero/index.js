import React, { useState } from "react";
import { calcTimeDelta } from "react-countdown";
import dynamic from "next/dynamic";
import { Display } from "./display";
import { Spacer } from "../../units/spacer";

const GOAL_DATE = "2020-10-08T00:01:00"; // Date.now() + 5000;

const SafeSSRCountdown = dynamic(
  () => import("react-countdown").then((mod) => mod),
  {
    loading: () => <p>Loading...</p>,
    ssr: false,
  }
);
// if (typeof window !== 'undefined') { }

const HeroSection = () => {
  const [stats, _] = useState(() => timeRemaining(GOAL_DATE));

  return (
    <aside className="pxy section-xl">
      <h1 className="-orange">OUVERTUREDES INSCRIPTIONS !</h1>
      <p>Le 08 octobre 2020</p>
      <Spacer size="l" />
      <SafeSSRCountdown date={GOAL_DATE} renderer={Display} />
      <p role="timer" className="offscreen">
        {stats.days} days left to the event
      </p>
    </aside>
  );
};

function timeRemaining() {
  const { total, days, hours, minutes, seconds, completed } = calcTimeDelta(
    GOAL_DATE
  );
  return { total, days, hours, minutes, seconds, completed };
}

export { HeroSection };
