import React, { useState } from "react";
import { Input, Form, CheckBox, compactObject } from "../../units/forms";
import { Action } from "../../units/action";
import { Spacer } from "../../units/spacer";

const emptyState = {
  email: "",
  nom: "",
  prenom: "",
  company: "",
  role: "",
  agree: false,
};
const Premium = () => {
  const [formValues, setFormValues] = useState(() => emptyState);
  const [feedback, setFeedback] = useState({});

  const update = (vals) => setFormValues((prev) => ({ ...prev, ...vals }));

  const saveToDb = () => {
    if (!formValues.agree) {
      setFeedback({ type: "agree", msg: "Please check me" });
    } else {
      setFeedback({});
    }
    // const nonEmptyValues = compactObject(formValues);
  };

  return (
    <>
      <Form
        a11yTitle="premium ticket"
        report={(vals) => update(vals)}
        formId={"premium-form"}
        server={feedback}
        tracking={formValues}
        className="form-grid"
      >
        <Input.Username name="nom" label="Nom" cantBeBlank />
        <Input.Username name="prenom" label="Prénom" cantBeBlank />
        <Input.Email name="email" label="Email" />
        <Input name="company" label="Societé" cantBeBlank />
        <Input name="role" label="Post Occupé" cantBeBlank />

        <CheckBox name="agree" label="GDPR Compliant" />
        {feedback && (
          <p aria-live="polite" input-error="">
            {feedback.msg}
          </p>
        )}
      </Form>
      <Spacer />

      <Action
        theme="TRANSPARENT"
        className="traced -orange "
        type="button"
        onPress={saveToDb}
      >
        Send
      </Action>
    </>
  );
};

export { Premium };
