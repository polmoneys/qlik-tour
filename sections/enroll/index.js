import React, { useState } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs"; // ***

// SECTIONS
import { Summary } from "./summary";
import { Standard } from "./standard";
import { Premium } from "./premium";
import { Spacer } from "../../units/spacer";
import Shape from "../../units/shape";

function EnrollSection(props) {
  const [tabIndex, setTabIndex] = useState(0);

  const handleTabsChange = (index) => {
    setTabIndex(index);
  };

  return (
    <section id="experience" className="section-l">
      <ExperienceCard active={tabIndex} />

      <Tabs
        className="lobotomized-owl "
        index={tabIndex}
        onChange={handleTabsChange}
      >
        <TabList>
          <Tab
            className="h2"
            style={{
              opacity: tabIndex != 0 ? 1 : 0,
            }}
          >
            &times;
          </Tab>
          {/* <Tab>Standard</Tab>
          <Tab>Premium</Tab> */}
        </TabList>
        <TabPanels>
          <TabPanel>
            <Summary cb={handleTabsChange} />
          </TabPanel>
          <TabPanel>
            <Standard />
          </TabPanel>
          <TabPanel>
            <Premium />
          </TabPanel>
        </TabPanels>
      </Tabs>
      <Spacer />
    </section>
  );
}

const ExperienceCard = ({ active }) => (
  <Shape sides={5} className="experience-card" size={200} fill="var(--orange)">
    {label(active)}
  </Shape>
);

const label = (id) => {
  if (id === 0)
    return (
      <>
        <p>CHOISIR SON</p>
        <h2 className="h2 font-bold font-center">EXPERIENCE</h2>
      </>
    );
  if (id === 1)
    return (
      <>
        <p>Entree</p>
        <h2 className="h2 font-bold font-center">STANDARD</h2>
      </>
    );
  if (id === 2)
    return (
      <>
        <p>Entree</p>
        <h2 className="h2 font-bold font-center">PREMIUM</h2>
      </>
    );
};

export { EnrollSection };
