import React from "react";
import { Action } from "../../units/action";
import { Spacer } from "../../units/spacer";

const Summary = ({ cb }) => {
  return (
    <div className="flex-landscape main-between cross-start">
      <div className="ticket-card self owl flex col cross-start main-center">
        <p className="h2 font-bold">ENTREE STANDARD</p>
        <p>Petit déjeneur</p>
        <p>Plenière</p>
        <p>Déjeneur</p>
        <p>Workshops au choix x2</p>
        <p>Cocktail de cloture</p>
        <Action
          theme="ACCENT"
          theme="TRANSPARENT"
          className="traced -orange self-push-top"
          onPress={() => cb(1)}
        >
          JE PARTICIPE
        </Action>
      </div>
      <div className="ticket-card self owl flex col cross-start main-center">
        <p className="h2 font-bold">ENTREE PREMIUM</p>
        <p>Place de parking au sous-sol</p>
        <p>Petit déjeneur</p>
        <p>Plenière</p>
        <p>Déjeneur</p>
        <p>Rendez-vous privé 20min</p>
        <p>Formations x2</p>
        <p>Cocktail de cloture</p>

        <Action
          theme="TRANSPARENT"
          className="traced -orange self-push-top"
          type="button"
          onPress={() => cb(2)}
        >
          JE PARTICIPE
        </Action>
      </div>
    </div>
  );
};

export { Summary };
