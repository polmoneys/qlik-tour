import React, { useState } from "react";
import { Input, Form, CheckBox, compactObject } from "../../units/forms";
import { Action, ActionInGroup, ActionGroup } from "../../units/action";
import { Spacer } from "../../units/spacer";
import { ReactSortable } from "react-sortablejs";

const emptyState = {
  email: "",
  nom: "",
  prenom: "",
  company: "",
  role: "",
  workshop: "",
  agree: false,
};

const Standard = () => {
  const [formValues, setFormValues] = useState(() => emptyState);
  const [feedback, setFeedback] = useState({});

  const [state, setState] = useState([
    { id: 1, name: "Workshop 1" },
    { id: 2, name: "Workshop 2" },
    { id: 3, name: "Workshop 3" },
    { id: 4, name: "Workshop 4" },
  ]);

  const update = (vals) => setFormValues((prev) => ({ ...prev, ...vals }));

  const saveToDb = () => {
    console.log(state);
    if (!formValues.agree) {
      setFeedback({ type: "agree", msg: "Please check me" });
    } else {
      setFeedback({});
    }
    //  const nonEmptyValues = compactObject(formValues);
  };

  const selectWorkshop = (item, fn) => {
    setFormValues((prev) => ({ ...prev, workshop: item }));
    fn(item);
  };

  return (
    <>
      <Form
        a11yTitle="standard ticket"
        report={(vals) => update(vals)}
        formId={"standard-form"}
        server={feedback}
        tracking={formValues}
        className="form-grid"
      >
        <Input.Username name="nom" label="Nom" cantBeBlank />
        <Input.Username name="prenom" label="Prénom" cantBeBlank />
        <Input.Email name="email" label="Email" />
        <Input name="company" label="Societé" cantBeBlank />
        <Input name="role" label="Post Occupé" cantBeBlank />
        <CheckBox name="agree" label="GDPR Compliant" />
        {feedback && (
          <p aria-live="polite" input-error="">
            {feedback.msg}
          </p>
        )}
      </Form>
      <Spacer />

      <p>Préférences sur les workshops proposés:</p>
      <Spacer />

      <ReactSortable list={state} setList={setState}>
        {state.map((item) => (
          <div className="sortable-card traced" key={item.id}>
            {item.name}
          </div>
        ))}
      </ReactSortable>

      <Spacer />
      <Action
        theme="TRANSPARENT"
        className="traced -orange "
        type="button"
        onPress={saveToDb}
      >
        Send
      </Action>
    </>
  );
};

export { Standard };
