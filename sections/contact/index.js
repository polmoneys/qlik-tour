import React, { useState } from "react";
import { NewsletterSection } from "./newsletter";
import { Input, Form, CheckBox, compactObject } from "../../units/forms";
import { Action } from "../../units/action";
import { Spacer } from "../../units/spacer";

const emptyState = {
  email: "",
  fullname: "",
  agree: false,
  query: "",
};
function ContactSection() {
  const [formValues, setFormValues] = useState(() => emptyState);
  const [feedback, setFeedback] = useState({});

  const update = (vals) => setFormValues((prev) => ({ ...prev, ...vals }));

  const saveToDb = () => {
    if (!formValues.agree) {
      setFeedback({ type: "agree", msg: "Please check me" });
    } else {
      setFeedback({});
    }
    //  const nonEmptyValues = compactObject(formValues);
  };

  return (
    <section id="contact" className="section-xs">
      <h2 className="h2 font-bold">NOUS CONTACTER</h2>
      <Spacer />
      <Form
        a11yTitle="contact event managers"
        report={(vals) => update(vals)}
        formId={"contact-form"}
        server={feedback}
        tracking={formValues}
      >
        <Input.Username name="fullname" label="Full name" cantBeBlank />
        <Input.Email name="email" label="Email" />
        <Input name="query" label="Question" cantBeBlank />

        <CheckBox name="agree" label="GDPR Compliant" />
        {feedback && (
          <p aria-live="polite" input-error="">
            {feedback.msg}
          </p>
        )}
      </Form>

      <Action
        theme="TRANSPARENT"
        className="traced -orange "
        type="button"
        onPress={saveToDb}
      >
        Send
      </Action>
    </section>
  );
}

export { ContactSection, NewsletterSection };
