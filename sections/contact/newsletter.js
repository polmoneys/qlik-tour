import React, { useState } from "react";
import { Input, Form, CheckBox, compactObject } from "../../units/forms";
import { Action } from "../../units/action";
import { Spacer } from "../../units/spacer";

const emptyState = {
  email: "",
  agree: false,
};

function NewsletterSection(props) {
  const [formValues, setFormValues] = useState(() => emptyState);
  const [feedback, setFeedback] = useState({});

  const update = (vals) => setFormValues((prev) => ({ ...prev, ...vals }));

  const saveToDb = () => {
    if (!formValues.agree) {
      setFeedback({ type: "agree", msg: "Please check me" });
    } else {
      setFeedback({});
    }
    //  const nonEmptyValues = compactObject(formValues);
  };

  return (
    <div className="section-xs">
      <p className="h3">Sign up for the newsletter</p>
      <Spacer />
      <Form
        a11yTitle="newsletter sign up"
        report={(vals) => update(vals)}
        formId={"sign-up"}
        server={feedback}
        tracking={formValues}
        theme="light"
      >
        <Input.Email name="email" label="Email" />
        <CheckBox name="agree" label="GDPR Compliant" />
        {feedback && (
          <p aria-live="polite" input-error="">
            {feedback.msg}
          </p>
        )}
      </Form>
      <Action
        theme="TRANSPARENT"
        className="traced -orange"
        type="button"
        onPress={saveToDb}
      >
        Send
      </Action>
      <Spacer />
    </div>
  );
}

export { NewsletterSection };
