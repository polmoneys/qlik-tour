import React from "react";

import { Speakers } from "./speakers";
import { Program } from "./program";
import { Summary } from "./summary";

function EventSection() {
  return (
    <>
      <Summary />
      <Speakers />
      <Program />
    </>
  );
}

export { EventSection };
