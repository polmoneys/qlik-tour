import React from "react";
import { Spacer } from "../../units/spacer";
import Shape from "../../units/shape";

const VenueSection = () => {
  return (
    <section id="practique" className="section-xl">
      <Spacer />
      <p className="font-center">
        Nous vous donnons rendez-vous le 08 octobre 2020 dans les locaux de
        CROSS à Genève, situés à l’adresse suivante :{" "}
      </p>
      <Spacer />
      <address>
        Route des Acacias 45B <LineBreak />
        1227 Genève
        <LineBreak />
        3ième étage{" "}
      </address>
      <div className="self owl section-s">
        <p className="font-bold">Train</p>
        <p>
          Pour les personnes voyageant en train, l’arrêt Pont-Rouge se situe à 5
          minutes à pied de nos locaux.{" "}
        </p>
        <p className="font-bold">Tram</p>
        <p>
          L’arrêt de bus et tram Pont-Rouge Étoile se situe à 2 minutes à pied
          de nos locaux. Les lignes de tram 15 et 17 s’y rendent directement,
          ainsi que les bus 21, 43, D, J et K.
        </p>
        <p className="font-bold">Voiture</p>
        <p>
          Vous avez la possibilité de déposer vos véhicules au Parking P+R
          Étoile qui se situe à 2 minutes à pied des locaux de CROSS. Quelques
          places de parking payantes sont disponibles en face de nos bureaux, à
          la route des Acacias 45B 1227 Acacias. Pour les participants ayant des
          entrées « Premium », des places de parking sont à votre disposition
          gratuitement au sous-sol de nos locaux. Votre numéro de place vous
          sera communiqué ultérieurement.{" "}
        </p>
      </div>
      <Spacer />
      <MapCard />
    </section>
  );
};

const MapCard = () => (
  <Shape.Circle className="map-card" size={200} fill="var(--orange)">
    <a
      href="https://www.google.es/maps/place/Route+des+Acacias+45B,+1227+Gen%C3%A8ve,+Switzerland/@46.1889485,6.1266968,17z/data=!3m1!4b1!4m5!3m4!1s0x478c7b2f87a97065:0xb19ba27b69c05ad2!8m2!3d46.1889485!4d6.1288908"
      target="_blank"
      rel="noopener noreferer"
    >
      <h2 className="h2 font-bold font-center">S'Y RENDRE</h2>
      <p className="font-center">Open map</p>
    </a>
  </Shape.Circle>
);

const LineBreak = () => <br aria-hidden="true" />;

export { VenueSection };

//  <div className="flex-landscape col-gap -xl">
//         <div className="self owl">
//   </div>
//         <div className="self -l pxy-landscape">
//           <img src='/map.png' alt="Event location" />
//         </div>
//       </div>
