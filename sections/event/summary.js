import React from "react";
import { Spacer } from "../../units/spacer";

const Summary = () => (
  <section id="summary" className="main-center cross-center">
    <Intro />
    <Spacer size="xl" />

    <StatsBar />
  </section>
);

const Intro = () => {
  return (
    <div className="section-s owl">
      <h2 className="h2 font-bold">QLIK TOUR SUISSE</h2>
      <p>
        {" "}
        Vos reportings ne vous permettent plus de comprendre votre activité ? Il
        est grand temps d’initier la révolution dataviz au sein de vos métiers.
        La data visualisation n’est pas l’accessoire de mode du pilotage dont
        les couleurs changeraient d’une saison à l’autre. Puissant outil
        analytics basé sur les concepts cognitifs, nous sommes convaincus chez
        CROSS et Qlik qu’il s’agit de la clé pour vous approprier vos données et
        les rendre intelligentes et intelligibles.
      </p>
      <p>
        {" "}
        A travers cette journée 100% Data qui aura lieu le 08 octobre aux côtés
        de QLIK et des équipes CROSS dans les locaux de CROSS à Genève, apprenez
        à libérer la valeur de la donnée, démocratisez son accès et faire en
        sorte qu’elle réponde enfin à vos interrogations métier !
      </p>
      <p>
        En savoir plus sur{" "}
        <a href="" target="_blank" rel="noreferer noopener">
          {" "}
          CROSS
        </a>{" "}
        et{" "}
        <a href="" target="_blank" rel="noreferer noopener">
          {" "}
          Qlik
        </a>
        .
      </p>
    </div>
  );
};
const StatsBar = () => (
  <div className="flex grid section-l">
    <StatCard>
      <strong className="h2">50</strong>
      <p>participants</p>
    </StatCard>
    <StatCard>
      <strong className="h2">10</strong>

      <p>orateurs</p>
    </StatCard>
    <StatCard>
      <strong className="h2">1</strong>

      <p>jour dédié</p>
    </StatCard>
    <StatCard>
      <strong className="h2">2</strong>
      <p>keynote</p>
    </StatCard>
    <StatCard>
      <strong className="h2">3</strong>

      <p>workshops</p>
    </StatCard>
    <StatCard>
      <strong className="h2">2</strong>

      <p>formations</p>
    </StatCard>
  </div>
);

const StatCard = ({ children }) => (
  <div className="stat-card self flex col main-end cross-center">
    {children}
  </div>
);

export { Summary };

//  <Shape.Circle fill="var(--orange)" size={120}>
//     <strong className="h2">2</strong>
//     </Shape.Circle>

const PALETTE = {
  pink: "rgb(244,190,212)",
  orange: "rgb(228,64,55)",
  purple: "rgb(99,91,199)",
  purpleDark: "rgb(20,19,65)",
  yellow: "rgb(255, 240, 155)",
};
