import React from "react";
import { Spacer } from "../../units/spacer";
import Shape from "../../units/shape";

const Program = () => {
  return (
    <section id="program" className="section-s ">
      <DownloadCard />
      <Row>
        <time dateTime="2022-10-08T09:15:00">9:15</time>
        <h2>ACCUEIL ET PETIT-DEJEUNER​</h2>
      </Row>
      <Row>
        <time dateTime="2022-10-08T10:00:00">10:00</time>
        <div>
          <h2>PLENIERE</h2>
          <p>
            Profiter de ce temps de pause pour échanger avec nos experts de vos
            problématiques​
          </p>
          <p>
            L'heure de votre rendez-vous privé vous sera communiqué en amont et
            aura lieu durant la pause déjeuner​
          </p>
        </div>
      </Row>

      <Row>
        <time dateTime="2022-10-08T12:30:00">12:30</time>
        <div>
          <h2>WORKSHOPS ET FORMATIONS​</h2>
          <p className="font-bold">WORKSHOP #1 - Durée: 45min </p>
          <p>Description du WORKSHOP 1 </p>
          <p className="font-bold">WORKSHOP #2 - Durée: 45min </p>
          <p>Description du WORKSHOP 2 </p>
          <p className="font-bold">WORKSHOP #3 - Durée: 45min </p>
          <p>Description du WORKSHOP 3 </p>
          <Spacer />
          <p className="font-bold">FORMATION #1 - Durée: 1h </p>
          <p>Description du formation 1 </p>
          <p className="font-bold">FORMATION #2 - Durée: 1h </p>
          <p>Description du formation 2 </p>
        </div>
      </Row>

      <Row>
        <time dateTime="2022-10-08T16:00:00">16:00</time>
        <h2>AFTERWORK​</h2>
      </Row>
    </section>
  );
};

const DownloadCard = () => (
  <Shape.Square className="download-card" size={200} fill="var(--orange)">
    <a href="" target="_blank" rel="noopener noreferer">
      <h2 className="h2 font-bold font-center">PROGRAM</h2>
      <p>Download</p>
    </a>
  </Shape.Square>
);

const Row = ({ children }) => (
  <div className="program-row flex main-between cross-baseline">{children}</div>
);
export { Program };
