import React from "react";
import { Pic } from "../../units/pic";
import { Ico } from "../../units/icon";
import { Spacer } from "../../units/spacer";
import Shape from "../../units/shape";
import Shape3D from "../../units/shape/3D";

const Speakers = () => {
  return (
    <section id="speakers">
      <div className="flex grid section-xl pxy">
        <CardFeatured />
        {SPEAKERS_LIST.map((speaker) => (
          <Card {...speaker} key={speaker.id} />
        ))}
      </div>
    </section>
  );
};

const CardFeatured = () => <Card featured />;

const Card = ({ portrait, name, bio, role, linkedin, featured }) => {
  if (featured)
    return (
      <div className="flex speaker-card main-center cross-center traced owl pxy -purple">
        <Shape.Triangle size={200} fill="var(--purple-dark)">
          <h2 className="h2 font-bold font-center">LES ORATEURS</h2>
        </Shape.Triangle>

        {/* <Shape3D.Billboard  shades={PALETTE.orange} borderColor={PALETTE.purpleDark} scaling={1} size={60}/> */}
      </div>
    );
  if (!name)
    return <div className="speaker-card placeholder" aria-hidden="true" />;
  return (
    <div className="speaker-card traced owl pxy">
      <div className="flex main-between">
        <Pic.AvatarXL src={portrait} />
        <a href={linkedin} target="_blank" rel="noopener noreferrer">
          <Ico color="var(--purple)" s />
          <span className="offscreen">Go to {name} Linkedin</span>
        </a>
      </div>
      <p className="font-bold -purple-dark">{name}</p>
      <p>{role}</p>
      <p>{bio}</p>
    </div>
  );
};

export { Speakers };

const PALETTE = {
  pink: "rgb(244,190,212)",
  orange: "rgb(228,64,55)",
  purple: "rgb(99,91,199)",
  purpleDark: "rgb(20,19,65)",
  yellow: "rgb(255, 240, 155)",
};

const SPEAKERS_LIST = [
  {
    id: 0,
    priority: 0,
    portrait: "",
    name: "Yann SESE",
    role: "Directeur DATA chez CROSS",
    bio: "Animateur formation modélisation",
    linkedin: "https://www.linkedin.com/in/seseyann/",
  },
  {
    id: 1,
    priority: 1,
    portrait: "",
    name: "Matthieu BUREL",
    role: "Smart Analytics manager chez Micropole et Qlik Luminary",
    bio:
      "Animation formation QlikSense /  Dynamic View et l'On-Demand App Generation",
    linkedin: "https://www.linkedin.com/in/matthieuburel/",
  },
  {
    id: 2,
    priority: 2,
    portrait: "",
    role: "Directeur DATA chez CROSS",
    name: "John Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 3,
    priority: 3,
    portrait: "",
    role: "Directeur DATA chez CROSS",
    name: "Jane Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 4,
    priority: 4,
    portrait: "",
    role: "Directeur DATA chez CROSS",
    name: "John Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 5,
    priority: 5,
    portrait: "",
    role: "Directeur DATA chez CROSS",
    name: "Jane Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 6,
    priority: 6,
    role: "Directeur DATA chez CROSS",
    portrait: "",
    name: "John Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 7,
    priority: 7,
    role: "Directeur DATA chez CROSS",
    portrait: "",
    name: "Jane Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },

  {
    id: 9,
    priority: 9,
    role: "Directeur DATA chez CROSS",
    portrait: "",
    name: "Jane Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 10,
    priority: 10,
    portrait: "",
    role: "Directeur DATA chez CROSS",
    name: "John Doe",
    bio: "Lorem ipsun dolor sit amet indie festival gloria at ea",
    linkedin: "https://linkedin.com",
  },
  {
    id: 11,
    priority: 11,
  },
];
